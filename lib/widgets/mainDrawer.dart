import 'dart:math';

import 'package:ablecoldworker/utils/images.dart';
import 'package:ablecoldworker/view/constants/globalComponents.dart';
import 'package:flutter/material.dart';

class MainDrawer extends StatefulWidget {
  @override
  _MainDrawerState createState() => _MainDrawerState();
}

class _MainDrawerState extends State<MainDrawer> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 0),
      child: Drawer(child: listDrawerItems(true)),
    );
  }

  listDrawerItems(bool bool) {
    return ListView(
      children: <Widget>[
        ListTile(
          trailing: IconButton(
            icon: Icon(Icons.close),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
        ListTile(
          leading: Container(
            height: 60,
            width: 60,
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Image.asset(
              Images.userPic,
              fit: BoxFit.fitHeight,
            ),
          ),
          title: Text(
            "Gaurav Thakur",
            style: TextStyle(fontSize: 20),
          ),
          subtitle: Text(
            "View Profile",
            style: TextStyle(fontSize: 14),
          ),
          onTap: () {},
        ),
        horizontalDivider,
        ListTile(
          leading: Icon(Icons.home, color: Colors.black),
          title: Text(
            "Home",
            style: TextStyle(fontSize: 18),
          ),
        ),
        horizontalDivider,
        ListTile(
          leading: Icon(Icons.shopping_cart, color: Colors.black),
          title: Text(
            "Picking Jobs",
            style: TextStyle(fontSize: 18),
          ),
        ),
        horizontalDivider,
        ListTile(
          leading: Transform.rotate(
            angle: pi/2,
            child: Icon(Icons.swap_horizontal_circle_outlined,
                color: Colors.black),
          ),
          title: Text(
            "Sorting Jobs",
            style: TextStyle(fontSize: 18),
          ),
        ),
        horizontalDivider,
        ListTile(
          leading: Icon(Icons.close_fullscreen, color: Colors.black),
          title: Text(
            "Inward Jobs",
            style: TextStyle(fontSize: 18),
          ),
        ),
        horizontalDivider,
        ListTile(
          leading: Icon(Icons.open_in_full, color: Colors.black),
          title: Text(
            "Putaway Jobs",
            style: TextStyle(fontSize: 18),
          ),
        ),
        horizontalDivider,
        ListTile(
          leading: Image.asset(Images.fleetIcon, color: Colors.black),
          title: Text(
            "Audit Jobs",
            style: TextStyle(fontSize: 18),
          ),
        ),
        horizontalDivider,
        ListTile(
          leading: Icon(Icons.notifications, color: Colors.black),
          title: Text(
            "Notification",
            style: TextStyle(fontSize: 18),
          ),
        ),
        horizontalDivider,
      ],
    );
  }
}
