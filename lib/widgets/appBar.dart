import 'package:ablecoldworker/constants/colorCodes.dart';
import 'package:ablecoldworker/utils/Images.dart';
import 'package:flutter/material.dart';

class MyAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String pageTitle;
  MyAppBar({Key key, this.pageTitle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new AppBar(
      elevation: 0.0,
      title: (pageTitle == null)
          ? Image.asset(
              Images.logo,
              width: (0.55) * MediaQuery.of(context).size.width,
            )
          : Text(pageTitle),
      centerTitle: true,
      backgroundColor: ColorCodes.colorPrimary,
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(kToolbarHeight);
}
