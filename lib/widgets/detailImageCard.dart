import 'package:flutter/material.dart';

class DetailImageCard extends StatefulWidget {
  final String youareatstring;
  final String palletstring;
  final String imgurl;
  final String imagetitle;
  final String mfgdatestring;
  final String expirydatestring;

  DetailImageCard(
      {Key key,
      this.youareatstring,
      this.palletstring,
      this.imgurl,
      this.imagetitle,
      this.mfgdatestring,
      this.expirydatestring})
      : super(key: key);

  @override
  _DetailImageCardState createState() => _DetailImageCardState();
}

class _DetailImageCardState extends State<DetailImageCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            color: Colors.white,
            padding: EdgeInsets.only(left: 15, top: 10, bottom: 10),
            child: Column(
              children: [
                IntrinsicHeight(
                  child: Row(
                    children: [
                      Text("You are At ",
                      style: TextStyle(
                        color: Colors.grey
                      ),
                      ),
                      Text(widget.youareatstring,
                      style: TextStyle(
                        fontWeight: FontWeight.bold
                      ),
                      ),
                      SizedBox(
                        width: 7,
                      ),
                      VerticalDivider(
                        thickness: 2,
                        width: 20,
                        color: Colors.grey,
                      ),
                      SizedBox(
                        width: 7,
                      ),
                      Text("Pallet- ",
                        style: TextStyle(
                            color: Colors.grey,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                      Text(widget.palletstring,
                      style: TextStyle(
                          fontWeight: FontWeight.bold
                      ),
                      ),
                    ],
                  ),
                ),
                widget.imgurl == null
                    ? Container(
                        padding: EdgeInsets.only(top: 90, bottom: 90),
                        child: Text("No image to display"))
                    : Container(
                        padding: EdgeInsets.only(top: 15, bottom: 10),
                        child: Image.network(widget.imgurl)),
                Container(
                  padding:
                      EdgeInsets.only( right: 15, top: 10, bottom: 10),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            widget.imagetitle,
                            style: TextStyle(fontWeight: FontWeight.bold,
                            fontSize: 18
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Text(
                                "MFG Date :-",
                                style: TextStyle(color: Colors.grey),
                              ),
                              SizedBox(width: 5),
                              Text(
                                widget.mfgdatestring,
                                style: TextStyle(fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                "Expiry Date :-",
                                style: TextStyle(color: Colors.grey),
                              ),
                              SizedBox(width: 5),
                              Text(
                                widget.expirydatestring,
                                style: TextStyle(fontWeight: FontWeight.bold),
                              )
                            ],
                          )
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          Divider()
        ],
      ),
    );
  }
}
