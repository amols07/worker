import 'dart:ui';

import 'package:flutter/material.dart';

class ConstantStrings {

  //location id textformfield decoration
  static final orifcannotfindbarcodetext = "Or if cannot find bar-code";
  static  final totalquantitytext="Total Quantity";
  static  final unitspcktstext="Units(pkts)";
  static  final otherissuestext="Other Issues";
  static  final partialpicktextstext="Partial Pick";
  static  final damagedquantitytext="Damaged Quantity";
  static  final missingquantitytext="Missing Quantity";
  static  final addcommentstext="Add Comments";
  static  final notenoughspacetext=" Not Enough Space";

  static  final invoicenumbertext="Invoice Number :";
  static  final vehicleidtext="Vehicle ID :";

  static  final temperaturetext="Temperature :";
  static  final jobidtext="Job ID :";


  // button texts
  static  final cancelbuttontext="Cancel";
  static  final donebuttontext="Done";


  //small textfields width and height
  static final double smalltextfieldheight=35;
  static final double smalltextfieldwidth=70;
  static final double checkiconsize=45;

}
