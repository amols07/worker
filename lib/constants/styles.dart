import 'dart:ui';

import 'package:ablecoldworker/constants/colorCodes.dart';
import 'package:flutter/material.dart';

class DecorationStyles {

   //location id textformfield decoration
  static final locationfielddecoration =   InputDecoration(
    hintText: "Enter Location ID",
    fillColor: Colors.white,
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(5.0),
      borderSide: BorderSide(
        color: Colors.blue,
      ),
    ),
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(5.0),
      borderSide: BorderSide(
        color: Colors.grey[100],
        width: 2.0,
      ),
    ),
  );
  static  final barCodeIcon=Icon(
    Icons.qr_code_sharp,
    color: Colors.white,
  );
  static final smalltextfieldsdecoration=InputDecoration(
    fillColor: Colors.white,
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(5.0),
      borderSide: BorderSide(
        color: Colors.blue,
      ),
    ),
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(5.0),
      borderSide: BorderSide(
        color: Colors.grey[100],
        width: 2.0,
      ),
    ),
  );
  static final addcommentstextfielddecoration=InputDecoration(
    fillColor: Colors.white,
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(5.0),
      borderSide: BorderSide(
        color: Colors.blue,
      ),
    ),
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(5.0),
      borderSide: BorderSide(
        color: Colors.grey[100],
        width: 2.0,
      ),
    ),
  );
  static final roundbuttondoneshape=RoundedRectangleBorder(
  borderRadius: BorderRadius.circular(18.0),
  side: BorderSide(
  color: ColorCodes.colorPrimary,
  )
  );
  static final roundbuttoncancelshape=RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(18.0),
      side: BorderSide(
        color: ColorCodes.colorSecondary,
      )
  );


}
