import 'package:flutter/material.dart';

final horizontalDivider = Container(
  height: 1,
  color: Colors.grey,
);

final verticalDivider = Container(
  width: 1,
  color: Colors.grey,
);
