import 'package:ablecoldworker/constants/colorCodes.dart';
import 'package:ablecoldworker/constants/constantStings.dart';
import 'package:ablecoldworker/constants/styles.dart';
import 'package:ablecoldworker/widgets/appBar.dart';
import 'package:ablecoldworker/widgets/detailImageCard.dart';
import 'package:ablecoldworker/widgets/mainDrawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

class DropProductDetails extends StatefulWidget {
  @override
  _DropProductDetailsState createState() => _DropProductDetailsState();
}

class _DropProductDetailsState extends State<DropProductDetails> {
  TextEditingController locationIdController = TextEditingController();

  yesButtonAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = RaisedButton(
      onPressed: () {},
      color: ColorCodes.colorSecondary,
      shape: DecorationStyles.roundbuttoncancelshape,
      child: Text(
        "No",
        style: TextStyle(color: Colors.black),
      ),
    );
    Widget continueButton = RaisedButton(
      onPressed: () {

      },
      color: ColorCodes.colorPrimary,
      shape: DecorationStyles.roundbuttondoneshape,
      child: Text(
        "Yes",
        style: TextStyle(color: Colors.white),
      ),
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(
        "Mark As Done",
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      content: Text(
          "Do you want to submit the details and mark it as done?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  noButtonAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = RaisedButton(
      onPressed: () {},
      color: ColorCodes.colorSecondary,
      shape: DecorationStyles.roundbuttoncancelshape,
      child: Text(
        ConstantStrings.cancelbuttontext,
        style: TextStyle(color: Colors.black),
      ),
    );
    Widget continueButton = RaisedButton(
      onPressed: () {

      },
      color: ColorCodes.colorPrimary,
      shape: DecorationStyles.roundbuttondoneshape,
      child: Text(
        ConstantStrings.donebuttontext,
        style: TextStyle(color: Colors.white),
      ),
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(
        "Cancel Alert",
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      content: Text(
          "Do you want to perform this picking later?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  // barcode function
  Future<void> scanBarcodeNormal() async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          "#ff6666", "Cancel", true, ScanMode.BARCODE);
      print(barcodeScanRes);
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      locationIdController.text = barcodeScanRes=="-1"?null:barcodeScanRes;
      print("lk" + barcodeScanRes);
      print("kl" + locationIdController.text);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: MyAppBar(
        pageTitle: "Drop Product Detail",
      ),
      drawer: MainDrawer(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            DetailImageCard(
              youareatstring: "CH-7",
              palletstring: "7",
              imagetitle: "Milky Mist Cooking Butter",
              mfgdatestring: "22/10/2018",
              expirydatestring: "21/10/2020",
              imgurl:
              "https://assetscdn1.paytm.com/images/catalog/product/F/FA/FASMILKY-MIST-CBIGB985832D2B753CB/1561515690954_0.jpg?imwidth=320&impolicy=hq",
            ),
            Container(
              height: 70,
              padding:
              EdgeInsets.only(left: 25, right: 25, top: 15, bottom: 10),
              child: RaisedButton(
                onPressed: () {
                  scanBarcodeNormal();
                  setState(() {
                    //locationIdController.text=null;
                  });
                },
                color: ColorCodes.colorPrimary,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    DecorationStyles.barCodeIcon,
                    SizedBox(
                      width: 7,
                    ),
                    Text(
                      "Scan Drop Area " + "|" + " Pallet - 7",
                      style: TextStyle(color: Colors.white),
                    )
                  ],
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 10, bottom: 10),
              child: Text(ConstantStrings.orifcannotfindbarcodetext),
            ),
            //location id controller
            Row(
              children: [
                Container(
                    height: 80,
                    width: MediaQuery.of(context).size.width-60,
                    padding:
                    EdgeInsets.only(top: 10, bottom: 10, left: 21, right: 21),
                    child: TextFormField(
                        controller: locationIdController,
                        decoration:DecorationStyles.locationfielddecoration
                    )),
                InkWell(
                  onTap: (){

                  },
                  child: Icon(
                    Icons.check_circle,
                    color: Colors.green,
                    size: ConstantStrings.checkiconsize,
                  ),
                )
              ],
            ),
            Container(
                width: MediaQuery.of(context).size.width,
                height: 30,
                color: Colors.grey[100],
                padding: EdgeInsets.only(left: 5, right: 5),
                child: Center(
                  child: Text(
                    'You have to Drop 5 Packets',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                )),
            //Total quantity
            Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: [
                  Text(
                    ConstantStrings.totalquantitytext,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(bottom: 10, left: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    ConstantStrings.unitspcktstext,
                  ),
                  Container(
                    width: ConstantStrings.smalltextfieldwidth,
                    height: ConstantStrings.smalltextfieldheight,
                    child: TextFormField(
                        decoration: DecorationStyles.smalltextfieldsdecoration,
                        keyboardType: TextInputType.number
                    ),
                  )
                ],
              ),
            ),
            Divider(),
            //Other issues
            Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: [
                  Text(
                    ConstantStrings.otherissuestext,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),

            Container(
              padding: EdgeInsets.only(bottom: 10, left: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    ConstantStrings.notenoughspacetext,
                  ),
                  Container(
                    width: ConstantStrings.smalltextfieldwidth,
                    height: ConstantStrings.smalltextfieldheight,
                    child: TextFormField(
                        decoration:  DecorationStyles.smalltextfieldsdecoration,
                        keyboardType: TextInputType.number
                    ),
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(bottom: 10, left: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    ConstantStrings.damagedquantitytext,
                  ),
                  Container(
                    width: ConstantStrings.smalltextfieldwidth,
                    height: ConstantStrings.smalltextfieldheight,
                    child: TextFormField(
                        decoration: DecorationStyles.smalltextfieldsdecoration,
                        keyboardType: TextInputType.number
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            //Add Comments
            Container(
              padding: EdgeInsets.all(10),
              child: Row(
                children: [
                  Text(
                    ConstantStrings.addcommentstext,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                ],
              ),
            ),
            Container(
                height: 80,
                padding: EdgeInsets.only(bottom: 10, left: 10, right: 10),
                child: TextFormField(
                  decoration:DecorationStyles.addcommentstextfielddecoration,
                )),

            //buttons
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                RaisedButton(
                  onPressed: () {
                    noButtonAlertDialog(context);
                  },
                  color: ColorCodes.colorSecondary,
                  shape: DecorationStyles.roundbuttoncancelshape,
                  child: Text(
                    ConstantStrings.cancelbuttontext,
                    style: TextStyle(color: Colors.black),
                  ),
                ),
                SizedBox(
                  width: 15,
                ),
                RaisedButton(
                  onPressed: () {
                    yesButtonAlertDialog(context);
                  },
                  color: ColorCodes.colorPrimary,
                  shape: DecorationStyles.roundbuttondoneshape,
                  child: Text(
                    ConstantStrings.donebuttontext,
                    style: TextStyle(color: Colors.white),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
