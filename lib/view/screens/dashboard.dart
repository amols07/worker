import 'package:ablecoldworker/utils/images.dart';
import 'package:ablecoldworker/view/constants/constants.dart';
import 'file:///C:/Users/AMOL/AndroidStudioProjects/clnn/ablecold-worker-app/lib/view/screens/Picking%20Module/Pick%20Products/pickingJobs.dart';
import 'package:ablecoldworker/widgets/appBar.dart';
import 'package:ablecoldworker/widgets/backgroundPainter.dart';
import 'package:ablecoldworker/widgets/mainDrawer.dart';
import 'package:flutter/material.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  dashboardTile(String title, String subtitle, Color color, String icon) {
    return InkWell(
      onTap: (){
        Navigator.push (
          context,
          MaterialPageRoute(builder: (context) => PickingJobs()),
        );

      },
      child: Card(
        elevation: 5.0,
        color: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        child: ListTile(
          leading: Container(
            height: 50,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: color,
            ),
            child: Image.asset(
              icon,
              color: Colors.white,
            ),
          ),
          trailing: Icon(Icons.chevron_right),
          title: Text(
            title,
            style: TextStyle(fontSize: 16),
          ),
          subtitle: Text(
            subtitle,
            style: TextStyle(
              fontSize: 25,
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          onTap: () {},
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(),
      drawer: MainDrawer(),
      body: CustomPaint(
        painter: BackgroundPainter(),
        child: Padding(
          padding: EdgeInsets.all(10.0),
          child: ListView(
            children: <Widget>[
              Card(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 16),
                      child: Column(
                        children: [
                          Text(
                            "310",
                            style: TextStyle(
                              fontSize: 25,
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            "Total Jobs",
                            style: TextStyle(
                              fontSize: 11,
                              color: Colors.black,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: 2,
                      height: 35,
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        shape: BoxShape.rectangle,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 16),
                      child: Column(
                        children: [
                          Text(
                            "125",
                            style: TextStyle(
                              fontSize: 25,
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            "Complete Jobs",
                            style: TextStyle(
                              fontSize: 11,
                              color: Colors.black,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: 2,
                      height: 35,
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        shape: BoxShape.rectangle,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 16),
                      child: Column(
                        children: [
                          Text(
                            "185",
                            style: TextStyle(
                              fontSize: 25,
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            "Active Jobs",
                            style: TextStyle(
                              fontSize: 11,
                              color: Colors.black,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              dashboardTile("Picking", "120", Colors.green, Images.fleetIcon),
              dashboardTile("Sorting", "40", Colors.yellow, Images.fleetIcon),
              dashboardTile("Inward", "54", Colors.red, Images.fleetIcon),
              dashboardTile("Putaway", "67", Colors.blue, Images.fleetIcon),
              dashboardTile("Audit", "39", Colors.blue[900], Images.fleetIcon),
            ],
          ),
        ),
      ),
    );
  }
}
