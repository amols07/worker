import 'file:///C:/Users/AMOL/AndroidStudioProjects/clnn/ablecold-worker-app/lib/view/screens/Picking%20Module/Pick%20Products/productDetail.dart';
import 'package:ablecoldworker/constants/constantStings.dart';
import 'package:ablecoldworker/widgets/stageCard.dart';
import 'package:ablecoldworker/widgets/totalJobsCard.dart';
import 'package:ablecoldworker/widgets/appBar.dart';
import 'package:ablecoldworker/widgets/backgroundPainter.dart';
import 'package:ablecoldworker/widgets/mainDrawer.dart';
import 'package:flutter/material.dart';

class PutawayJobs extends StatefulWidget {
  @override
  _PutawayJobsState createState() => _PutawayJobsState();
}

class _PutawayJobsState extends State<PutawayJobs> {
  String _status = "In Progress";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: MyAppBar(
        pageTitle: "Putaway Jobs",
      ),
      drawer: MainDrawer(),
      body: CustomPaint(
        painter: BackgroundPainter(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.all(10.0),
                child:                TotalJobsCard(
                  totaljobs: "310",
                  totaljobslabel: "Total jobs",
                  activejobs: "185",
                  activeobslabel: "Active Jobs",
                ),
//                StageCard(
//                  stagenumber: "STAGE #1",
//                  textbelowstage: "you have total 13 products to pick",
//                  jobid: "Job ID:76776",
//                  status: "NOT STARTED",
//                ),

              ),
              ListView.builder(
                primary: false,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: 6,
                itemBuilder: (context, i) {
                  return InkWell(
                    onTap: (){
                      Navigator.push (
                        context,
                        MaterialPageRoute(builder: (context) => ProductDetails()),
                      );
                    },
                    child: Card(
                      elevation: 5.0,
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Column(
                        children: [
                          ListTile(
                            leading: Icon(Icons.person,size: 50,),
                            title: Row(
                              children: [
                                Row(
                                  children: [
                                    Text(
                                      "Rohit Sharma",
                                    ),
                                  ],
                                ),

                              ],
                            ),

                          ),
                          Divider(),
                          Container(
                            padding: EdgeInsets.only(),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                            ConstantStrings.invoicenumbertext
                                        ),
                                        SizedBox(width: 5,),
                                        Text(
                                          '87687',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(),
                                    Row(
                                      children: [
                                        Text(
                                            ConstantStrings.vehicleidtext
                                        ),
                                        SizedBox(width: 5,),
                                        Text(
                                          '786',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                                SizedBox(height: 10,),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                            ConstantStrings.temperaturetext
                                        ),
                                        SizedBox(width: 5,),
                                        Text(
                                          '0 degree         ',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.symmetric(vertical: 16),
                                child: Column(
                                  children: [
                                    Text(
                                      "Job ID",
                                      style: TextStyle(
                                        fontSize: 15,
                                        color: Colors.black,
                                      ),
                                    ),
                                    Text(
                                      "87675",
                                      style: TextStyle(
                                        fontSize: 17,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                width: 2,
                                height: 35,
                                decoration: BoxDecoration(
                                  color: Colors.grey,
                                  shape: BoxShape.rectangle,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(vertical: 16),
                                child: Column(
                                  children: [
                                    Text(
                                      "Total Pallets",
                                      style: TextStyle(
                                        fontSize: 15,
                                        color: Colors.black,
                                      ),
                                    ),
                                    Text(
                                      "5",
                                      style: TextStyle(
                                        fontSize: 17,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                width: 2,
                                height: 35,
                                decoration: BoxDecoration(
                                  color: Colors.grey,
                                  shape: BoxShape.rectangle,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(vertical: 16),
                                child: Column(
                                  children: [
                                    Text(
                                      "Status",
                                      style: TextStyle(
                                        fontSize: 15,
                                        color: Colors.black,
                                      ),
                                    ),
                                    Text(
                                      "Active Jobs",
                                      style: TextStyle(
                                        fontSize: 17,
                                        color: _status == "In Progress"
                                            ? Colors.red
                                            : Colors.yellow,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
