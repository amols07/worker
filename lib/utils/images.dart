class Images {
  static final onboarding = "assets/images/onboarding.png";
  static final logo = "assets/images/logo.png";
  static final fleetIcon = "assets/images/fleetic.png";
  static final userPic = "assets/images/user_profile_pic.png";
}
