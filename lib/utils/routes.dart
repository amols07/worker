import 'package:ablecoldworker/auth/login.dart';
import 'package:ablecoldworker/view/screens/Audit%20Module/auditproductdetail.dart';
import 'package:ablecoldworker/view/screens/Inward%20Module/inwardjobs.dart';
import 'package:ablecoldworker/view/screens/Putaway%20Module/putawayjobs.dart';
import 'package:ablecoldworker/view/screens/Sorting%20Module/sortingjobs.dart';
import 'file:///C:/Users/AMOL/AndroidStudioProjects/clnn/ablecold-worker-app/lib/view/screens/Picking%20Module/Pick%20Products/pickingJobs.dart';
import 'package:ablecoldworker/view/screens/dashboard.dart';

final routes = {
  '/': (context) => new Login(),
  '/dashboard': (context) => new Dashboard(),
  '/picking': (context) => new PickingJobs(),
  '/audit': (context) => new AuditProductDetails(),
  '/inward': (context) => new InwardJobs(),
  '/putaway': (context) => new PutawayJobs(),
  '/sorting': (context) => new SortingJobs(),

};
