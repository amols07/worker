import 'package:ablecoldworker/constants/colorCodes.dart';
import 'package:ablecoldworker/utils/images.dart';
import 'package:ablecoldworker/utils/validation.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final TextEditingController emailTextController = TextEditingController();
  final TextEditingController passwordTextController = TextEditingController();

  bool valuefirst = false;
  final validate = ValidateField();

  @override
  void dispose() {
    emailTextController.dispose();
    passwordTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorCodes.colorPrimary,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(Images.onboarding),
            colorFilter: new ColorFilter.mode(
                ColorCodes.colorPrimary.withOpacity(0.2), BlendMode.dstATop),
            alignment: Alignment.bottomCenter,
          ),
        ),
        child: Form(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                Images.logo,
                alignment: Alignment.bottomCenter,
                width: MediaQuery.of(context).size.width * 0.7,
              ),
              SizedBox(height: 30),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white38,
                  borderRadius: BorderRadius.circular(25.0),
                ),
                width: MediaQuery.of(context).size.width * 0.8,
                child: TextFormField(
                  keyboardType: TextInputType.visiblePassword,
                  controller: emailTextController,
                  validator: (email) {
                    if (email == null || email.isEmpty) {
                      return "Please enter an Email ID";
                    } else if (validate.isEmail(email) == false) {
                      return "Invalid Email ID";
                    } else
                      return null;
                  },
                  style: TextStyle(
                    fontSize: 15.0,
                    color: Colors.white,
                  ),
                  cursorColor: Colors.white,
                  decoration: InputDecoration(
                    prefixIcon: Padding(
                      padding: EdgeInsets.only(right: 2),
                      child: Container(
                        height: 15,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.white),
                        child: Icon(
                          Icons.email_outlined,
                          color: ColorCodes.colorPrimary,
                        ),
                      ),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.0),
                      borderSide: BorderSide(color: Colors.white),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.0),
                      borderSide: BorderSide(color: Colors.white),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.0),
                      borderSide: BorderSide(color: Colors.white),
                    ),
                    contentPadding: EdgeInsets.all(10.0),
                    labelText: "Email Address",
                    labelStyle: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 20),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white38,
                  borderRadius: BorderRadius.circular(25.0),
                ),
                width: MediaQuery.of(context).size.width * 0.8,
                child: TextFormField(
                  keyboardType: TextInputType.visiblePassword,
                  controller: passwordTextController,
                  validator: (password) {
                    if (password == null || password.isEmpty) {
                      return "Please enter a Password";
                    } else
                      return null;
                  },
                  style: TextStyle(
                    fontSize: 15.0,
                    color: Colors.white,
                  ),
                  cursorColor: Colors.white,
                  decoration: InputDecoration(
                    prefixIcon: Padding(
                      padding: EdgeInsets.only(right: 2),
                      child: Container(
                        height: 15,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.white),
                        child: Icon(
                          Icons.lock_outline,
                          color: ColorCodes.colorPrimary,
                        ),
                      ),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.0),
                      borderSide: BorderSide(color: Colors.white),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.0),
                      borderSide: BorderSide(color: Colors.white),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.0),
                      borderSide: BorderSide(color: Colors.white),
                    ),
                    contentPadding: EdgeInsets.all(10.0),
                    labelText: "Password",
                    labelStyle: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 20),
              Container(
                width: MediaQuery.of(context).size.width * 0.9,
                child: CheckboxListTile(
                  value: this.valuefirst,
                  onChanged: (val) {
                    setState(() {
                      this.valuefirst = val;
                    });
                  },
                  subtitle: !this.valuefirst
                      ? Text(
                          'Required.',
                          style: TextStyle(fontSize: 13.0, color: Colors.red),
                        )
                      : null,
                  title: Text(
                    'I agree with the Terms & Conditions.',
                    style: TextStyle(fontSize: 13.0, color: Colors.white),
                  ),
                  controlAffinity: ListTileControlAffinity.leading,
                  activeColor: Colors.green,
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25.0),
                ),
                width: MediaQuery.of(context).size.width * 0.7,
                child: RaisedButton(
                  color: Colors.white,
                  child: Text(
                    "LOGIN",
                    style: TextStyle(
                      fontSize: 18.0,
                      color: ColorCodes.colorPrimary,
                    ),
                  ),
                  onPressed: () {
                    Navigator.pushReplacementNamed(context, '/dashboard');
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
